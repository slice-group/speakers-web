class CreateConferences < ActiveRecord::Migration
  def change
    create_table :conferences do |t|
      t.string :city
      t.string :place
      t.string :dates
      t.string :hours

      t.timestamps null: false
    end
  end
end
