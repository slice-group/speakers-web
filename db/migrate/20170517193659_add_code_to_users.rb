class AddCodeToUsers < ActiveRecord::Migration
  def change
    add_column :users, :code, :string

    User.find_each do |user|
      user.update code: code_generator
      puts "Codigo para #{user.email}: #{user.code}" if user.is_admin?
    end
  end

  private
  def code_generator
    code = loop do
      random_code = SecureRandom.hex(5)
      break random_code unless User.find_by(code: random_code)
    end
  end
end
