class RemoveSavedFromPosts < ActiveRecord::Migration
  def change
    remove_column :posts, :saved, :boolean
  end
end
