class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :title
      t.text :body
      t.integer :user_id
      t.integer :post_category_id
      t.string :attachment
      t.boolean :public
      t.string :permalink
      t.boolean :saved

      t.timestamps null: false
    end
  end
end
