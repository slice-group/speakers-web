class UnpublishPostsWithBody < ActiveRecord::Migration
  def change
    Post.find_each do |post|
      post.update(body: nil, public: false)
    end
  end
end
