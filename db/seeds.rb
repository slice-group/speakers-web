[:admin, :speaker].each do |name|
  rol = Role.new name: name
  puts "#{name} has been created" if rol.save
end

user = User.new(
  name: 'Admin', email: 'admin@keppler.com', password: '12345678',
  password_confirmation: '12345678', role_ids: '1'
)
puts 'admin@keppler.com has been created' if user.save

if SocialAccount.count.zero?
  # Create setting deafult
  setting = Setting.new(
    name: 'Keppler admin', description: 'Welcome to Keppler Admin',
    smtp_setting_attributes: {
      address: 'test', port: '25', domain_name: 'keppler.com',
      email: 'info@keppler.com', password: '12345678'
    },
    google_analytics_setting_attributes: {
      ga_account_id: '60688852',
      ga_tracking_id: 'UA-60688852-1',
      ga_status: true
    }
  )
  setting.social_account = SocialAccount.new(
    facebook: "https://www.facebook.com/speaker.gustavohenao?fref=ts",
    twitter: "https://twitter.com/GustavoHenaoS",
    instagram: "https://instagram.com/GustavoHenao"
  )
  setting.appearance = Appearance.new(theme_name: 'keppler')
  puts 'Setting default has been created' if setting.save
end

if KepplerContactUs::MessageSetting.count.zero?
  message_setting = KepplerContactUs::MessageSetting.new(
    mailer_to: 'example@example.com',
    mailer_from: 'example@example.com'
  )
  puts 'MessageSetting saved.' if message_setting.save
end

if Conference.count.zero?
  Conference.create city: "Venezuela (Valencia)",
                    place: "Hotel LIDOTEL",
                    dates: "Del 5 al 9 de Julio",
                    hours: "De 8:00 a.m. a 12:00 p.m / de 2:00 p.m. a 6:00 p.m..."
  Conference.create city: "Colombia (Medellin)",
                    place: "NH COLLECTION",
                    dates: "Del 22 al 26 de Noviembre"
end
