# Preview all emails at http://localhost:3000/rails/mailers/user_mailer
class UserMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/user_mailer/after_create
  def after_create
    UserMailer.after_create(
      user: User.last,
      password: '12345678'
    )
  end

end
