require 'test_helper'

class UserMailerTest < ActionMailer::TestCase
  test "after_create" do
    mail = UserMailer.after_create
    assert_equal "After create", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
