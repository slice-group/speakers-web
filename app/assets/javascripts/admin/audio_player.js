function player(id) {
  var music = document.getElementById('audio-'+id); // id for audio element
  var btnPlay = document.getElementById('btn-play-'+id);
  var btnStop = document.getElementById('btn-stop-'+id);
  var btnRewind = document.getElementById('btn-rewind-'+id);
  var btnForward = document.getElementById('btn-forward-'+id);
  var btnVolumeUp = document.getElementById('btn-volume-up-'+id);
  var btnVolumeDown = document.getElementById('btn-volume-down-'+id);
  var btnVolumeMute = document.getElementById('btn-volume-mute-'+id);
  var duration = music.duration;
  var playhead = document.getElementById('playhead-'+id);
  var timeline = document.getElementById('audio-timeline-'+id);

  // timeline width adjusted for playhead
  var timelineWidth = timeline.offsetWidth - playhead.offsetWidth;
  // play button event listenter
  btnPlay.addEventListener("click", playPause);
  btnStop.addEventListener("click", stop);
  btnRewind.addEventListener("click", rewind);
  btnForward.addEventListener("click", forward);
  btnVolumeUp.addEventListener("click", volumeUp);
  btnVolumeDown.addEventListener("click", volumeDown);
  btnVolumeMute.addEventListener("click", volumeMute);

  //Play and Pause
  function playPause() {
      if (music.paused) {
        play()
      } else {
        pause();
      }
  }
  function play() {
    music.play();
    $('#btn-play-'+id+' i').text("pause");
  }
  function pause() {
    music.pause();
    $('#btn-play-'+id+' i').text("play_arrow");
  }
  function stop() {
    music.currentTime = 0;
    pause();
  }
  function rewind() {
    music.currentTime = music.currentTime - 10;
  }
  function forward() {
    music.currentTime = music.currentTime + 10;
  }
  function volumeUp() {
    var i = music.volume + 0.2;
    if (i < 1) { music.volume = i; }
    if (music.paused) { play() }
  }
  function volumeDown() {
    var i = music.volume - 0.2;
    if (i > 0) { music.volume = i; }
    else {
      music.volume = 0;
      pause();
    }
  }
  function volumeMute() {
    if (music.volume === 0){
      music.volume = 1;
      play();
    } else {
      music.volume = 0;
      pause();
    }
  }

  // timeupdate event listener
  music.addEventListener("timeupdate", timeUpdate, false);

  // makes timeline clickable
  timeline.addEventListener("click", function(event) {
      moveplayhead(event);
      music.currentTime = duration * clickPercent(event);
  }, false);

  // returns click as decimal (.77) of the total timelineWidth
  function clickPercent(event) {
      return (event.clientX - getPosition(timeline)) / timelineWidth;
  }

  // makes playhead draggable
  playhead.addEventListener('mousedown', mouseDown, false);
  window.addEventListener('mouseup', mouseUp, false);

  // Boolean value so that audio position is updated only when the playhead is released
  var onplayhead = false;

  // mouseDown EventListener
  function mouseDown() {
      onplayhead = true;
      window.addEventListener('mousemove', moveplayhead, true);
      music.removeEventListener('timeupdate', timeUpdate, false);
  }

  // mouseUp EventListener
  // getting input from all mouse clicks
  function mouseUp(event) {
      if (onplayhead == true) {
          moveplayhead(event);
          window.removeEventListener('mousemove', moveplayhead, true);
          // change current time
          music.currentTime = duration * clickPercent(event);
          music.addEventListener('timeupdate', timeUpdate, false);
      }
      onplayhead = false;
  }
  // mousemove EventListener
  // Moves playhead as user drags
  function moveplayhead(event) {
      var newMargLeft = event.clientX - getPosition(timeline);

      if (newMargLeft >= 0 && newMargLeft <= timelineWidth) {
          playhead.style.marginLeft = newMargLeft + "px";
      }
      if (newMargLeft < 0) {
          playhead.style.marginLeft = "0px";
      }
      if (newMargLeft > timelineWidth) {
          playhead.style.marginLeft = timelineWidth + "px";
      }
  }

  // timeUpdate
  // Synchronizes playhead position with current point in audio
  function timeUpdate() {
      var playPercent = timelineWidth * (music.currentTime / duration);
      playhead.style.marginLeft = playPercent + "px";
      if (music.currentTime == duration) {
          pButton.className = "";
          pButton.className = "play";
      }
  }


  // Gets audio file duration
  music.addEventListener("canplaythrough", function() {
      duration = music.duration;
  }, false);

  // getPosition
  // Returns elements left position relative to top-left of viewport
  function getPosition(el) {
      return el.getBoundingClientRect().left;
  }
}

