$(document).on('ready page:load', function() {
  $('input').each(function(){
    moveLabel(this);
  });

  $('input').on('focus', function() {
    $(this).parent().find('label').addClass('active');
  });

  $('input').on('blur', function() {
    moveLabel(this);
  });
  $('.blog .btn-search').on('click', function() {
    if($('.blog-search').hasClass('active')) {
      $('.blog-search').removeClass('active');
      $('.btn-search i').html('search')
    } else {
      $('.blog-search').addClass('active');
      $('.btn-search i').html('close');
      $('.nav-search').focus();
    }
  });
});

function moveLabel(input) {
  if ($(input).val() !== '') {
    $(input).parent().find('label').addClass('active');
  } else {
    $(input).parent().find('label').removeClass('active');
  }
}
