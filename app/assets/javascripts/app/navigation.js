function removeHash () {
  history.pushState("", document.title, window.location.pathname + window.location.search);
}
$( document ).ready(function() {

  $('.top-nav').onePageNav({
    currentClass: 'current',
    changeHash: false,
    scrollSpeed: 1000,
    filter: ':not(.external)',
    begin: function() {
      removeHash();
    },
    end: function() {},
    scrollChange: function($currentListItem) {}
  });

  var urls = {
    1: "url('assets/app/headerperfil.jpg')",
    2: "url('assets/app/headerspeaker.jpg')",
    3: "url('assets/app/headergaleria.png')"
  };

  $(".headerpeaker").css("background-image", urls[Math.floor((Math.random() * 3) + 1)]);
});
