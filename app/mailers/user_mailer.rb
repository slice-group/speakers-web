class UserMailer < ApplicationMailer
  #default from: 'gustavo@gustavohenao.com'

  def after_create(user, password, request_url)
    @user = user
    @password = password
    @token = set_password_token
    @request_url = request_url

    mail from: "no-reply@gustavohenao.com",
         subject: "Has sido invitado a Soyspeaker.com",
         to: @user.email
  end

  private

  def set_password_token
    raw, enc = Devise.token_generator.generate(User, :reset_password_token)
    @user.reset_password_token   = enc
    @user.reset_password_sent_at = Time.now.utc
    @user.save(validate: false)
    raw
  end
end
