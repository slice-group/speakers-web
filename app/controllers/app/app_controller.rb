module App
  # AppController -> Controller out the back-office
  class AppController < ::ApplicationController
    layout 'app/layouts/application'
    before_filter :set_metas

    def set_metas
      @setting = Setting.first
      @title = @setting.name
      @description = @setting.description
      @favicon = @setting.favicon
      @meta = MetaTag.get_by_url(request_url)
      @google_adword = GoogleAdword.get_by_url(request_url)
      @ga_tracking = google_analytics_tracking
    end

    private

    def google_analytics_tracking
      GoogleAnalyticsTrack.get_tracking_id(request_url).try(:tracking_id) ||
        @setting.google_analytics_setting.ga_tracking_id
    end

    def request_url
      url = request.url
      no_protocol = url.split('://')[1]
      variations = [url, no_protocol]
      if url[url.length - 1] == '/'
        variations << url[0...url.length - 1]
        variations << no_protocol[0...no_protocol.length - 1]
      else
        variations << "#{url}/"
        variations << "#{no_protocol}/"
      end
      variations
    end
  end
end
