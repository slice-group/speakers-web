module App
  # FrontController
  class FrontController < AppController
    before_action :conferences

    def index
      @images = Image.where(public: true).shuffle.take(3)
      @message = KepplerContactUs::Message.new
    end

    def gallery
      @images = Image.where(public: true)
      @message = KepplerContactUs::Message.new
    end

    private
    def conferences
      @conferences = Conference.all
      @social_accounts = SocialAccount.first
      @conferences_places = @conferences.map(&:place)
    end
  end
end
