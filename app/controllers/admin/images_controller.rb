module Admin
  # ImagesController
  class ImagesController < AdminController
    before_action :set_image, only: [:show, :edit, :update, :destroy]
    before_action :show_history, only: [:index]

    # GET /images
    def index
      @q = Image.ransack(params[:q])
      images = @q.result(distinct: true)
      @objects = images.page(@current_page)
      @total = images.size
      if !@objects.first_page? && @objects.size.zero?
        redirect_to images_path(page: @current_page.to_i.pred, search: @query)
      end
    end

    # GET /images/1
    def show
    end

    # GET /images/new
    def new
      @image = Image.new
    end

    # GET /images/1/edit
    def edit
    end

    # POST /images
    def create
      @image = Image.new(image_params)

      if @image.save
        redirect(@image, params)
      else
        render :new
      end
    end

    # PATCH/PUT /images/1
    def update
      if @image.update(image_params)
        redirect(@image, params)
      else
        render :edit
      end
    end

    def clone
      @image = Image.clone_record params[:image_id]

      if @image.save
        redirect_to admin_images_path
      else
        render :new
      end
    end

    # DELETE /images/1
    def destroy
      @image.destroy
      redirect_to admin_images_path, notice: actions_messages(@image)
    end

    def destroy_multiple
      Image.destroy redefine_ids(params[:multiple_ids])
      redirect_to(
        admin_images_path(page: @current_page, search: @query),
        notice: actions_messages(Image.new)
      )
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_image
      @image = Image.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def image_params
      params.require(:image).permit(:image, :public, :title)
    end

    def show_history
      get_history(Image)
    end
  end
end
