module Admin
  # BlogController
  class BlogController < AdminController
    POST_PER_PAGE = 6

    before_action :load_post, only: [:show, :save, :post_comment, :delete_comment]
    before_action :load_more_comments, only: [:show, :post_comment, :delete_comment]

    # GET /posts
    def blog
      @q = Post.ransack(params[:q])
      posts = @q.result(distinct: true).where(public: true).order(created_at: :desc)
      @posts = posts.page(@current_page).per(POST_PER_PAGE)
      @total = posts.size

      if !@posts.first_page? && @posts.size.zero?
        redirect_to(
          admin_blog_path(page: @current_page.to_i.pred, search: @query)
        )
      end
    end

    def search
      @filter = params[:filter] ? params[:filter] : {}
      @search = @filter.merge(params[:q]) if params[:q]
      @q = Post.ransack(@search)
      posts = @q.result(distinct: true)
      @posts = posts.page(@current_page).per(POST_PER_PAGE)
      @total = posts.size

      if !@posts.first_page? && @posts.size.zero?
        redirect_to(
          admin_search_path(page: @current_page.to_i.pred, filter: params[:filter], q: params[:q])
        )
      end
    end

    def show
      @comment = @post.comments.new
    end

    def saved_posts
      @posts = current_user.get_saved Post
    end

    def save
      if current_user.saved? @post
        @post.unsave_by current_user
      else
        @post.upsaved_by current_user
      end
    end

    def post_comment
      @comment = @post.comments.create(comments_params)
      @comment.save
    end

    def delete_comment
      comment = Comment.find(params[:comment_id])
      comment.destroy!
    end

    private

    def load_post
      @post = Post.find_by_permalink(params[:permalink])
    end

    def load_more_comments
      @next_comments = params[:next_comments] ? params[:next_comments].to_i : 10
      @comments = @post.comments.recent.limit(@next_comments)
      @comments_left = @post.comments.count - @next_comments
    end

    def comments_params
      {
        commentable_id: @post.id,
        commentable_type: "Post",
        user_id: current_user.id,
        comment: params[:comment]
      }
    end
  end
end
