module Admin
  # ConferencesController
  class ConferencesController < AdminController
    before_action :set_conference, only: [:show, :edit, :update, :destroy]
    before_action :show_history, only: [:index]

    # GET /conferences
    def index
      @q = Conference.ransack(params[:q])
      conferences = @q.result(distinct: true)
      @objects = conferences.page(@current_page)
      @total = conferences.size
      if !@objects.first_page? && @objects.size.zero?
        redirect_to admin_conferences_path(page: @current_page.to_i.pred, search: @query)
      end
    end

    # GET /conferences/1
    def show
    end

    # GET /conferences/new
    def new
      if Conference.count >= 3
        redirect_to admin_conferences_path,
                    notice: 'Solo pueden haber dos fechas de conferencias.\nEdite o elimine una.'
      end
      @conference = Conference.new

    end

    # GET /conferences/1/edit
    def edit
    end

    # POST /conferences
    def create
      @conference = Conference.new(conference_params)

      if @conference.save
        redirect(@conference, params)
      else
        render :new
      end
    end

    # PATCH/PUT /conferences/1
    def update
      if @conference.update(conference_params)
        redirect(@conference, params)
      else
        render :edit
      end
    end

    def clone
      @conference = Conference.clone_record params[:conference_id]

      if @conference.save
        redirect_to admin_conferences_path
      else
        render :new
      end
    end

    # DELETE /conferences/1
    def destroy
      @conference.destroy
      redirect_to admin_conferences_path, notice: actions_messages(@conference)
    end

    def destroy_multiple
      Conference.destroy redefine_ids(params[:multiple_ids])
      redirect_to(
        admin_conferences_path(page: @current_page, search: @query),
        notice: actions_messages(Conference.new)
      )
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_conference
      @conference = Conference.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def conference_params
      params.require(:conference).permit(:city, :place, :dates, :hours)
    end

    def show_history
      get_history(Conference)
    end
  end
end
