module Admin
  # PostCategoriesController
  class PostCategoriesController < AdminController
    before_action :set_post_category, only: [:show, :edit, :update, :destroy]
    before_action :show_history, only: [:index]

    # GET /post_categories
    def index
      @q = PostCategory.ransack(params[:q])
      post_categories = @q.result(distinct: true)
      @objects = post_categories.page(@current_page)
      @total = post_categories.size
      if !@objects.first_page? && @objects.size.zero?
        redirect_to post_categories_path(page: @current_page.to_i.pred, search: @query)
      end
    end

    # GET /post_categories/1
    def show
    end

    # GET /post_categories/new
    def new
      @post_category = PostCategory.new
    end

    # GET /post_categories/1/edit
    def edit
    end

    # POST /post_categories
    def create
      @post_category = PostCategory.new(post_category_params)

      if @post_category.save
        redirect(@post_category, params)
      else
        render :new
      end
    end

    # PATCH/PUT /post_categories/1
    def update
      if @post_category.update(post_category_params)
        redirect(@post_category, params)
      else
        render :edit
      end
    end

    # DELETE /post_categories/1
    def destroy
      @post_category.destroy
      redirect_to admin_post_categories_path, notice: actions_messages(@post_category)
    end

    def destroy_multiple
      PostCategory.destroy redefine_ids(params[:multiple_ids])
      redirect_to(
        admin_post_categories_path(page: @current_page, search: @query),
        notice: actions_messages(PostCategory.new)
      )
    end

    def clone
      @post_category = PostCategory.clone_record params[:post_category_id]

      if @post_category.save
        redirect_to admin_post_categories_path
      else
        render :new
      end
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_post_category
      @post_category = PostCategory.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def post_category_params
      params.require(:post_category).permit(:name)
    end

    def show_history
      get_history(PostCategory)
    end
  end
end
