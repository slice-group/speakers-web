module Admin
  # PostsController
  class PostsController < AdminController
    before_action :set_post, only: [:show, :edit, :update, :destroy]
    before_action :set_categories, only: [:new, :edit, :update, :create]
    before_action :show_history, only: [:index]

    # GET /posts
    def index
      @q = Post.ransack(params[:q])
      posts = @q.result(distinct: true).order(created_at: :desc)
      @objects = posts.page(@current_page)
      @total = posts.size
      if !@objects.first_page? && @objects.size.zero?
        redirect_to posts_path(page: @current_page.to_i.pred, search: @query)
      end
    end

    # GET /posts/1
    def show
      set_comments
    end

    # GET /posts/new
    def new
      @post = Post.new
    end

    # GET /posts/1/edit
    def edit
    end

    # POST /posts
    def create
      @post = Post.new(post_params.merge(user_id: current_user.id, permalink: @post.title.downcase.parameterize))

      if @post.save
        redirect(@post, params)
      else
        render :new
      end
    end

    # PATCH/PUT /posts/1
    def update
      if @post.update(post_params.merge(permalink: @post.title.downcase.parameterize))
        set_comments
        redirect(@post, params)
      else
        render :edit
      end
    end

    def clone
      @post = Post.clone_record params[:post_id]

      if @post.save
        redirect_to admin_posts_path
      else
        render :new
      end
    end

    # DELETE /posts/1
    def destroy
      @post.destroy
      redirect_to admin_posts_path, notice: actions_messages(@post)
    end

    def destroy_multiple
      Post.destroy redefine_ids(params[:multiple_ids])
      redirect_to(
        admin_posts_path(page: @current_page, search: @query),
        notice: actions_messages(Post.new)
      )
    end

    private

    def set_comments
      @comment = @post.comments.new
      @next_comments = params[:next_comments] ? params[:next_comments].to_i : 10
      @comments = @post.comments.recent.limit(@next_comments)
      @comments_left = @post.comments.count - @next_comments
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def post_params
      params.require(:post).permit(:title, :body, :user_id, :post_category_id, :attachment, :public, :permalink, :allow_comments)
    end

    def show_history
      get_history(Post)
    end

    def set_categories
      @categories = PostCategory.order(:name)
    end
  end
end
