module Admin
  # UsersController
  class UsersController < AdminController
    before_action :set_user, only: [:show, :edit, :update, :destroy]
    before_action :show_history, only: [:index]

    def index
      @q = User.ransack(params[:q])
      users = @q.result(distinct: true).where('id != ?', current_user.id)
      @objects = users.page(@current_page)
      @total = users.size
      if !@objects.first_page? && @objects.size.zero?
        redirect_to users_path(page: @current_page.to_i.pred, search: @query)
      end
    end

    def new
      if params[:rol]
        @user = User.new
        @rol = Role.find_by name: params[:rol]
      else
        redirect_to admin_users_path, notice: 'Debe seleccionar un rol para crear un usuario.'
      end
    end

    def show
    end

    def edit
      @rol = Role.find(@user.role_ids.first)
    end

    def update
      update_attributes = user_params.delete_if do |_, value|
        value.blank?
      end

      if @user.update_attributes(update_attributes)
        redirect(@user, params)
      else
        render action: 'edit'
      end
    end

    def create
      @rol_id = Role.last.id

      if user_params[:role_ids] == @rol_id.to_s
        password = Devise.friendly_token.first(8)
        @user = User.new(speaker_params(password))
      else
        @user = User.new(user_params)
      end

      if @user.save
        @user.add_role Role.find(user_params.fetch(:role_ids)).name
        UserMailer.after_create(@user, password, request.base_url).deliver_now if @user.is_speaker?
        redirect(@user, params)
      else
        @rol = Role.find(user_params.fetch(:role_ids))
        render action: 'new'
      end
    end

    def destroy
      @user.transaction do
        Comment.where(user: @user).destroy_all
        @user.destroy!
      end
      redirect_to admin_users_path, notice: actions_messages(@user)
    end

    def destroy_multiple
      User.destroy redefine_ids(params[:multiple_ids])
      redirect_to(
        admin_users_path(page: @current_page, search: @query),
        notice: actions_messages(User.new)
      )
    end

    private

    def speaker_params(password)
      { name: user_params[:name],
        email: user_params[:email],
        role_ids: user_params[:role_ids],
        password: password,
        password_confirmation: password
      }
    end

    def set_user
      @user = User.find(params[:id])
    end

    def user_params
      params.require(:user).permit(
        :name, :email, :password, :password_confirmation,
        :role_ids, :encrypted_password
      )
    end

    def show_history
      get_history(User)
    end

    # Get submit key to redirect, only [:create, :update]
    def redirect(object, commit)
      if commit.key?('_save')
        redirect_to admin_users_path, notice: actions_messages(object)
      elsif commit.key?('_add_other')
        redirect_to new_admin_user_path, notice: actions_messages(object)
      elsif commit.key?('_assing_rol')
        redirect_to admin_users_path, notice: actions_messages(object)
      end
    end
  end
end
