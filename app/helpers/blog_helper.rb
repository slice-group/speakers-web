# BlogHelper -> Helpers base
module BlogHelper
  def filter_name(filter)
    case filter
    when 'user_name_eq'
      'Autor'
    when 'created_at_eq'
      'Fecha de publicación'
    when 'post_category_name_eq'
      'Categoría'
    end
  end
end
