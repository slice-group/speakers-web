# Conference Model
class Conference < ActiveRecord::Base
  include ActivityHistory
  include CloneRecord

  validates_presence_of :city, :place, :dates

  # Fields for the search form in the navbar
  def self.search_field
    :city_or_place_or_dates_cont
  end
end
