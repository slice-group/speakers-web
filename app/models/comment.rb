class Comment < ActiveRecord::Base

  include ActsAsCommentable::Comment

  belongs_to :commentable, :polymorphic => true

  default_scope -> { order('created_at ASC') }

  validates_presence_of :comment
  validate :dont_post_duplicated_comment

  # NOTE: install the acts_as_votable plugin if you
  # want user to vote on the quality of comments.
  #acts_as_voteable

  # NOTE: Comments belong to a user
  belongs_to :user

  def dont_post_duplicated_comment
    if Comment.where('comment = ? and user_id = ? and created_at > ?', comment, user_id, (DateTime.now - 5.minutes)).any?
      errors.add :base, 'No duplicate'
    end
  end
end
