# Image Model
class Image < ActiveRecord::Base
  include ActivityHistory
  include CloneRecord

  mount_uploader :image, AttachmentUploader

  # Fields for the search form in the navbar
  def self.search_field
    :image_or_public_or_title_cont
  end
end
