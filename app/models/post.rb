# Post Model
require 'file_size_validator'
class Post < ActiveRecord::Base
  include ActivityHistory
  include CloneRecord

  acts_as_commentable
  acts_as_saveable

  mount_uploader :attachment, AttachmentUploader

  belongs_to :user
  belongs_to :post_category
  validates_presence_of :title, :post_category_id
  validates_uniqueness_of :title
  validates :attachment,
            presence: true,
            file_size: {
              maximum: 20.megabytes.to_i
            }

  # Fields for the search form in the navbar
  def self.search_field
    :title_or_body_or_user_name_or_post_category_name_cont
  end

  def self.published
    where(public: true).order(created_at: :desc)
  end

  def date
    created_at.strftime("%d %b. %Y")
  end
end
