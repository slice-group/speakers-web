# GoogleAnayticsTrack Model
class GoogleAnalyticsTrack < ActiveRecord::Base
  include ActivityHistory

  def self.get_tracking_id(url)
    find_by_url(url)
  end

  def self.search_field
    :name_or_tracking_id_cont
  end
end
