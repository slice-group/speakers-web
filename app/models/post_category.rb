# PostCategory Model
class PostCategory < ActiveRecord::Base
  include ActivityHistory
  include CloneRecord

  validates_uniqueness_of :name
  validates_presence_of :name

  has_many :posts

  # Fields for the search form in the navbar
  def self.search_field
    :name_cont
  end
end
