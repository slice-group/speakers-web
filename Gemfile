source 'https://rubygems.org'

gem 'rails', '4.2.0'

# Javascript
gem 'jquery-rails'
gem 'angularjs-rails'
gem 'nprogress-rails'
gem 'ckeditor'

# Documentation
gem 'sdoc', '~> 0.4.0', group: :doc

# Utilities
gem 'haml-rails'
gem 'coffee-rails', '~> 4.0.0'
gem 'sass-rails'
gem 'therubyracer', '~> 0.11.4', platforms: :ruby
gem 'uglifier', '>= 1.3.0'
gem 'jbuilder', '~> 2.0'
gem 'turbolinks', '~> 2.5.3'
gem 'jquery-turbolinks'

# Database
gem 'mysql2', '~> 0.3.18'
#gem 'pg'

# SEO
gem 'sitemap_generator'

# History
gem 'public_activity'

# Forms
gem 'simple_form'
gem 'cocoon'

# Upload
gem 'carrierwave'
gem 'rmagick'

# Desgin
gem 'bourbon'
gem 'bootstrap-sass', '~> 3.3.6'
gem 'font-awesome-sass', '~> 4.3.0'
gem 'material_icons'
gem 'materialize-sass'

# Authentication and Authorization
gem 'devise', '3.5.2'
gem 'devise-i18n'
gem 'cancancan'
gem 'rolify'

gem 'ransack', git: 'https://github.com/activerecord-hackery/ransack',
               branch: 'rails-4.2'

# Pagination
gem 'kaminari-i18n'
gem 'kaminari'

gem 'acts_as_saveable', '~> 0.10.1'
gem 'acts_as_commentable'

# Development
group :development, :test do
  gem 'better_errors'
  gem 'quiet_assets'
  gem 'rails_layout'
  gem 'table_print'
  gem 'spring'
  gem 'guard'
  gem 'letter_opener'
  # gem 'guard-sass'
  gem 'guard-coffeescript'
  gem 'guard-livereload'
  gem 'byebug'
  gem 'web-console', '~> 2.0'
  gem 'commands'
  # Style guides
  gem 'rubocop', require: false
  gem 'haml-lint', require: false
  gem 'scss_lint', require: false
end

#---- keppler_ga_dashboard ----
gem 'keppler_ga_dashboard', git: 'https://github.com/inyxtech/keppler_ga_dashboard.git'
gem 'google-api-client', '~> 0.7.1'

# ---- keppler-contact-us ----
gem 'keppler_contact_us', git: "https://github.com/inyxtech/keppler_contact_us.git", branch: 'update'
gem "recaptcha", require: "recaptcha/rails"
